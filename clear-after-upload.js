// clear the input after upload, so if you hit Back it won't still have the old file waiting for you to upload it by accident
window.addEventListener("pagehide", function() {
	var form = document.getElementById("form")
	form.reset()
})
