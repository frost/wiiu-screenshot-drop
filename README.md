# Wii U Screenshot Drop

Save screenshots from your Wii U!

### Usage

Start the server with `node main.js`.

Images will be saved in "~/Pictures/Wii U Screenshots" by default. Pass a different folder on the command line to change that, e.g. `node main.js ~/foo`.

Then, when you're playing a game:
- suspend the game
- open the Internet Browser
- go to your computer's IP/port 4000 (e.g. `http://10.0.1.8:4000`)
	- mDNS ("your-machine.local") does _not_ seem to work. Unfortunately.
	- You can look this up with `ifconfig` on Linux/Mac (or `ip addr` if you like the new shiny stuff), or `ipconfig` on Windows.
	- If the Wii U suddenly can't find your machine, the IP may have changed.
- upload the image (the Wii U will let you pick from TV or GamePad screenshots when you hit "browse")

### License

Public domain! See [the license](COPYING.md) for more information.
