let express = require("express")()
let expressStatic = require("serve-static") // comes with Express
let server = require("http").Server(express)
let path = require("path")

var fs = require("fs")

let util = require("util")
fs.readFilePromise = util.promisify(fs.readFile)
fs.writeFilePromise = util.promisify(fs.writeFile)

let Busboy = require("busboy")

var screenshotDir = path.normalize(require("os").homedir() + "/Pictures/Wii U Screenshots")

// Node.js argv: arguments start at argv[2]!
if (process.argv.length > 2) {
	screenshotDir = path.normalize(process.argv[2])
	console.log("Set screenshot dir to", screenshotDir)
}

express.use(expressStatic("."))

express.get("/upload", function(request, response) {
	console.log("GET /upload")
})
express.post("/upload", function(request, response) {
	// console.log("POST /upload")
	
	let busboy = new Busboy({headers: request.headers})
	busboy.on("file", function(fieldname, file, filename, encoding, mimetype) {
		// the Wii U doesn't return very good filenames! Let's make our own.
		// YYYY-MM-DD HH-MM-SS.jpg
		let extension = filename.replace(/.*(\..*$)/, "$1")
		let now = new Date()
		filename = now.getFullYear() + "-"
			+       String(now.getMonth()+1).padStart(2, "0")
			+ "-" + String(now.getDate()).padStart(2, "0")
			+ " " + String(now.getHours()).padStart(2, "0")
			+ "-" + String(now.getMinutes()).padStart(2, "0")
			+ "-" + String(now.getSeconds()).padStart(2, "0")
			+ extension
		
		let filepath = path.join(screenshotDir, filename)
		process.stdout.write("Saving to" + filepath + "...")
		let outfile = fs.createWriteStream(filepath)
		file.pipe(outfile)
		outfile.on("finish", function() {
			console.log("done!")
			response.redirect(303, "/done.html")
		})
		outfile.on("error", function() {
			console.log(" **ERROR**")
			response.redirect(303, "/error.html")
		})
	})
	busboy.on("field", function(fieldname, value) {
		console.log("Non-file field " + fieldname + ":", value)
	})
	busboy.on("finish", function() {
	})
	request.pipe(busboy)
})

server.listen(4000, function() {
	console.log("Server started, listening on port 4000")
})
